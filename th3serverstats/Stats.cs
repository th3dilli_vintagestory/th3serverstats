using System.Collections.Generic;

namespace Th3ServerStats
{
    public class ServerInfo
    {
        public string ServerName { get; set; }

        public string WorldName { get; set; }

        public string WorldType { get; set; }

        public int OnlinePlayers { get; set; }

        public int MaxPlayers { get; set; }

        public List<Player> Players { get; set; }
        public List<Mod> Mods { get; set; }
    }
}