using Vintagestory.API.Common;

namespace Th3ServerStats
{
    public class Mod
    {
        public string Name { get; set; }

        public string Version { get; set; }
        
        public string ModId { get; set; }
        public EnumAppSide Side { get; set; }
    }
}