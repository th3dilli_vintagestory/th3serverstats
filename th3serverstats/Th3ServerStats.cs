using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Vintagestory.API.Common;
using Vintagestory.API.Server;

namespace Th3ServerStats
{
    public class Th3ServerStats : ModSystem
    {
        private const string _configFile = "Th3ServerStatsConfig.json";

        private ICoreServerAPI _sapi;

        private HttpListener _httpL;

        private ServerInfo _serverInfo;

        private Th3ServerStatsConfig _config;

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Server;
        }

        public override void StartServerSide(ICoreServerAPI sapi)
        {
            _sapi = sapi;

            try
            {
                _config = _sapi.LoadModConfig<Th3ServerStatsConfig>(_configFile);

                if (_config == null)
                {
                    _config = new Th3ServerStatsConfig
                    {
                        Port = _sapi.Server.Config.Port + 1
                    };
                    _sapi.StoreModConfig(_config, _configFile);
                }
            }
            catch (Exception e)
            {
                _sapi.Logger.Error($"Error loading {_configFile}: " + e.StackTrace);
                return;
            }

            _serverInfo = new ServerInfo()
            {
                ServerName = _sapi.Server.Config.ServerName,
                MaxPlayers = _sapi.Server.Config.MaxClients,
                WorldName = _sapi.WorldManager.SaveGame.WorldName,
                WorldType = _sapi.WorldManager.SaveGame.WorldType
            };

            _sapi.Event.ServerRunPhase(EnumServerRunPhase.Shutdown, OnShutdown);

            _httpL = new HttpListener();
            _httpL.Prefixes.Add($"http://{_config.Host}:{_config.Port}/{_config.Endpoint}/");

            _httpL.Start();
            _ = Task.Run(AsyncListen);
        }

        private async Task AsyncListen()
        {
            while (_httpL.IsListening)
            {
                HttpListenerContext context = await _httpL.GetContextAsync();
                try
                {
                    await ProcessRequestAsync(context);
                }
                catch (Exception ex)
                {
                    _sapi.Logger.Error("Th3ServerStats  " + ex.StackTrace);
                }
            }
        }

        private async Task ProcessRequestAsync(HttpListenerContext context)
        {
            context.Response.KeepAlive = false;
            if (!context.Request.RawUrl.Equals($"/{_config.Endpoint}"))
            {
                context.Response.StatusCode = 400;
                context.Response.Close();
                return;
            }

            _serverInfo.Players = _sapi.World.AllOnlinePlayers.Select(p => new Player(p.PlayerName, ((IServerPlayer)p).Ping)).ToList();
            _serverInfo.OnlinePlayers = _sapi.World.AllOnlinePlayers.Length;

            _serverInfo.Mods = _sapi.ModLoader.Mods.Select(m =>
                new Mod()
                {
                    Name = m.Info.Name,
                    Version = m.Info.Version, 
                    ModId = m.Info.ModID,
                    Side = m.Info.Side
                }).ToList();
            
            var serverInfoString = JsonConvert.SerializeObject(_serverInfo);

            var bytes = Encoding.UTF8.GetBytes(serverInfoString);
            context.Response.StatusCode = 200;
            context.Response.ContentLength64 = bytes.Length;
            context.Response.ContentType = "application/json";
            var output = context.Response.OutputStream;
            await output.WriteAsync(bytes, 0, bytes.Length);
            output.Close();
            context.Response.Close();
        }

        private void OnShutdown()
        {
            _httpL.Stop();
        }
    }
}