# Th3ServerStats

This mod exposes a HTTP server that serves by default on `http://localhost:42421/stats` (if server port is 42420) server statistics in JSON format.
The default port used is game port + 1.

#### Side
- 1 Server
- 2 Client
- 3 Universal

```json
{
  "ServerName": "Vintage Story Server",
  "WorldName": "A new world",
  "WorldType": "standard",
  "OnlinePlayers": 1,
  "MaxPlayers": 16,
  "Players": [
    {
      "Name": "Th3Dilli",
      "Ping": 0.293
    }
  ],
  "Mods": [
    {
      "Name": "Essentials",
      "Version": "1.18.15",
      "ModId": "game",
      "Side": 3
    },
    {
      "Name": "Server Stats",
      "Version": "0.2.0",
      "ModId": "th3serverstats",
      "Side": 1
    },
    {
      "Name": "Creative Mode",
      "Version": "1.18.15",
      "ModId": "creative",
      "Side": 3
    },
    {
      "Name": "Survival Mode",
      "Version": "1.18.15",
      "ModId": "survival",
      "Side": 3
    }
  ]
}
```

This mod was created so that the TCAdmin interface can query server information.

## TCAdmin Custom Query Protocol

https://help.tcadmin.com/Custom_Query_Protocol

### IronPython Script

Make sure to change the localhost part in the `url` to match with what you set in the mod config.
If your server is not running on the same host you may also need to open that port but be aware that others can now also consume this JSON "API".

```py
import clr
import System
from System import String
import urllib2
import json

url = "http://localhost:" + str(ThisService.QueryPort) + "/stats"

response = urllib2.urlopen(url).read()
data =  json.loads(response)

XmlFormat="<?xml version=\"1.0\" encoding=\"UTF-8\"?><qstat><server type=\"CUSTOM\" address=\"{0}:{1}\" status=\"{2}\"><hostname>{0}:{1}</hostname><name>{3}</name><gametype>{4}</gametype><map>{5}</map><numplayers>{6}</numplayers><maxplayers>{7}</maxplayers><numspectators>0</numspectators><maxspectators>0</maxspectators><ping>0</ping><retries>0</retries><players>{8}</players></server></qstat>"
Status="UP"
Name=data["ServerName"]
GameType=data["WorldType"]
Map=data["WorldName"]
NumPlayers=data["OnlinePlayers"]
MaxPlayers=data["MaxPlayers"]
Players = ""

playerXML = "<player><name>{0}</name><ping>{1}</ping><score>0</score></player>"
for player in data["Players"]:
      Players = Players + String.Format(playerXML,player["Name"], player["Ping"])

ReturnValue=String.Format(XmlFormat, ThisService.IpAddress, ThisService.QueryPort, Status, Name, GameType, Map, NumPlayers, MaxPlayers, Players)
```
